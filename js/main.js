// url for banner images
$img1='https://www.toronto.ca/wp-content/uploads/2017/11/984e-cityplanning_scarboroughcentreonthemove_banner.jpg'
$img2 ='http://demo.bestdnnskins.com/portals/6/innerpage/banner3_04.jpg'
$img3 ='http://styles.prosites.com/litesite/2522/images/ban/banner5.jpg'


$(document).ready(function () {
    $("#imageID").attr('src', $img1)
})

// function to change the banner image
function callImage(x) {
    $(".banner-btn").css("background", "transparent")
    if(x===3){
        $("#imageID").attr('src', $img3)
        $("#banner-btn-3").css("background", "white")
    }else if(x==2){
        $("#imageID").attr('src', $img2)
        $("#banner-btn-2").css("background", "white")
    }else{
        $("#imageID").attr('src', $img1)
        $("#banner-btn-1").css("background", "white")
    }
}